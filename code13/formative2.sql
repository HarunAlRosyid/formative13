-- Insert table address  
INSERT INTO address (street, city, province, country, zipCode) 
VALUES 
	('Apple Park Way','Cupertino','California','United States',95014),
    ('Jl. Wismasari Sel. No.12','Semarang','Jawa Tengah','Indonesia',50181);
    
-- Insert table brands  
INSERT INTO brands (name) 
VALUES 
	('Advan'),
    ('Apple');
    
-- Insert table brands  
INSERT INTO manufacturers (name, addressId) 
VALUES 
	('Advan Manufacture',2),
    ('Apple Manufacture',1);
 
-- Insert table valutas  
INSERT INTO valutas (code, name) 
VALUES 
	('IDR', 'Rupiah'),
    ('USD', 'US Dollar');
    
-- Insert table products 
INSERT INTO products (artNumber, name, manufactureId, brandId, stock)
VALUES 
	(12345671, 'Ipad Air 16 GB',2,2,15),
    (12345672, 'Ipad Air 64 GB',2,2,14),
    (12345673, 'Ipad Pro 32 GB',2,2,13),
    (12345674, 'Ipad Pro 128 GB',2,2,0),
    (12345675, 'Ipad Pro 12.9 32 GB',2,2,11),
    (12345676, 'Ipad Pro 12.9 128 GB',2,2,12),
    (12345677, 'Iphone 6 64 GB',2,2,0),
    (12345678, 'Iphone SE 64 GB',2,2,3),
    (12345679, 'Apple Watch 38mm',2,2,4),
    (12345670, 'Apple Watch 42mm',2,2,5),
    (22345671, 'Advance G3',1,1,15),
    (22345672, 'Advance G3 pro',1,1,14),
    (22345673, 'Advance I6c',1,1,13),
    (22345674, 'Advance S5 Pro',1,1,0),
    (22345675, 'Advance S5',1,1,11),
    (22345676, 'Advance S4',1,1,12),
    (22345677, 'Advance S4 pro',1,1,0),
    (22345678, 'Advance S6 plus',1,1,3),
    (22345679, 'Advance I6',1,1,4),
    (22345670, 'Advance G2',1,1,5);
    
-- Insert table prices 
INSERT INTO prices (productId, valutaId, amount)
VALUES
	(1,2,399.00),
	(2,2,499.00),
	(3,2,599.00),
	(4,2,749.00),
	(5,2,799.00),
	(6,2,949.00),
	(7,2,749.00),
	(8,2,499.00),
	(9,2,299.00),
	(10,2,399.00),
	(11,1,1000000.00),
	(12,1,1100000.00),
	(13,1,1200000.00),
	(14,1,1300000.00),
	(15,1,1400000.00),
	(16,1,1500000.00),
	(17,1,1600000.00),
	(18,1,1700000.00),
	(19,1,1800000.00),
	(20,1,1900000.00);

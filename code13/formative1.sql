-- create database product management
CREATE DATABASE formativedb;

-- use database formativedb
USE formativedb;

-- Create table address
CREATE TABLE address (
	id INT(10) AUTO_INCREMENT PRIMARY KEY,
    street varchar(50) NULL,
	city varchar(50) NULL,
	province varchar(50) NULL,
	country varchar(50) NULL,
    zipCode INT(8) NULL
);

-- create table manufactur
CREATE TABLE manufacturers (
	id INT(10) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NULL,
    addressId INT(10) NULL,
		KEY addressId (`addressId`),
			CONSTRAINT addressId FOREIGN KEY (`addressId`) REFERENCES address (`id`)
);

-- create table brand
CREATE TABLE brands (
	id INT(10) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NULL
);

-- create table products
CREATE TABLE products (
	id INT(10) AUTO_INCREMENT PRIMARY KEY,
    artNumber INT(10) NULL,
    name VARCHAR(100) NULL,
	description VARCHAR(100) NULL,
    manufactureId INT(10) NULL,
    brandId INT(10) NULL,
    stock INT(15) NULL,
		KEY manufactureId (`manufactureId`),
			CONSTRAINT manufactureId FOREIGN KEY (`manufactureId`) REFERENCES manufacturers (`id`),
        KEY brandId (`brandId`),
			CONSTRAINT brandId FOREIGN KEY (`brandId`) REFERENCES brands (`id`)
);

-- create table valuta
CREATE TABLE valutas (
	id INT(10) AUTO_INCREMENT PRIMARY KEY,
    code VARCHAR(10) NULL,
    name VARCHAR(100) NULL
);

-- create table price
CREATE TABLE prices (
	id INT(10) AUTO_INCREMENT PRIMARY KEY,
    productId INT(10) NULL,
    valutaId INT(10) NULL,
    amount DECIMAL(20,2) NULL,
		KEY productId (`productId`),
			CONSTRAINT productId FOREIGN KEY (`productId`) REFERENCES products (`id`),
		KEY valutaId (`valutaId`),
			CONSTRAINT valutaId FOREIGN KEY (`valutaId`) REFERENCES valutas (`id`)
);






SELECT products.id , products.stock ,manufacturers.name as manufacturer,
	   address.street , address.city , address.province , address.country , address.zipCode ,
	   brands.name as brands , prices.amount, valutas.name, valutas.code as valuta FROM products 
		LEFT JOIN manufacturers ON manufacturers.id = products.manufactureId 
        LEFT JOIN address ON address.id = manufacturers.addressId 
		LEFT JOIN brands ON brands.id = products.brandId 
		LEFT JOIN prices ON prices.productId = products.id
		LEFT JOIN valutas ON prices.valutaId = valutas.id
        GROUP BY valutas.code , products.id 
		ORDER BY prices.amount DESC;
